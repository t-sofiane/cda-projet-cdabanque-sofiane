package fr.afpa.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import fr.afpa.beans.Agence;
import fr.afpa.beans.Client;
import fr.afpa.beans.Compte;
import fr.afpa.control.Control;
import fr.afpa.dao.Dao;

public class Model {
	
	public static void creerAgence(Agence agence) {
		Dao.creerAgence(agence);
		}
	
	public static void creerCompte(Compte compte) {
	Dao.creerCompte(compte);
	}
	
	public static void creerClient(Client client) {
		Dao.creerClient(client);

	}
	
	public static void rechercherCompte() {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		Scanner in= new Scanner(System.in);
		System.out.println("veuillez saisir le numéro de compte");
		int numCpte=in.nextInt();
		//in.hasNextLine();
		//
		try { // Charger et configurer le driver de la base de données.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			// Connection en utilisant l'user directement dans la methode getConnection
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "sofianeT", "St241181");
			// Requette à executer
			String strQuery = "SELECT * FROM compte where numCompte='"+numCpte+"'";
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);
			
			if (listeServices.next()) {
			// Utilisation du ResultSet
				System.out.println("numéro de  compte : " + listeServices.getInt(1));
				System.out.println("Solde             : " + listeServices.getFloat(2)+" €");
				System.out.println("Id client         : " + listeServices.getString(4));
				System.out.println("Code agence       : " + listeServices.getInt(5));

				System.out.println("---------------------------------------------------------");
			}else {
				System.out.println("   compte non trouvé!!");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}
	}
	
	public static void rechercherClient() {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		Scanner in= new Scanner(System.in);
		System.out.println("veuillez saisir l'id client");
		String idClt=in.nextLine();
		try { // Charger et configurer le driver de la base de données.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			// Connection en utilisant l'user directement dans la methode getConnection
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "sofianeT", "St241181");
			// Requette à executer
			String strQuery = "SELECT * FROM client where idClient='"+idClt+"'";
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);
			
			if (listeServices.next()) {
			// Utilisation du ResultSet
				if (listeServices.getBoolean("actif")) {
				System.out.println("Id client         : " + listeServices.getString(1));
				System.out.println("Nom               : " + listeServices.getString(2));
				System.out.println("Prenom            : " + listeServices.getString(3));
				System.out.println("Date de naissance : " + listeServices.getString(4));
				System.out.println("Adresse           : " + listeServices.getString(5));
				System.out.println("---------------------------------------------------------");
				}else {
				System.out.println("	Le client est désactivé!!!");
				}
			}else {
				System.out.println("   Client non trouvé!!");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}
	}
	
	public static void listCompteClient() {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		Scanner in= new Scanner(System.in);
		System.out.println("veuillez saisir l'id compte");
		String idClt=in.nextLine();
		//in.hasNextLine();
		//
		try { // Charger et configurer le driver de la base de données.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			// Connection en utilisant l'user directement dans la methode getConnection
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "sofianeT", "St241181");
			// Requette à executer
			String strQuery = "SELECT * FROM compte where idClient='"+idClt+"'";
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);

			//if (listeServices.next()) {
				while(listeServices.next()) {
			// Utilisation du ResultSet
				System.out.println("numéro de  compte : " + listeServices.getInt(1));
				System.out.println("Solde             : " + listeServices.getFloat(2)+" €");
				System.out.println("Id client         : " + listeServices.getString(4));
				System.out.println("Code agence       : " + listeServices.getInt(5));

				System.out.println("---------------------------------------------------------");
			}
//			}else {
//				System.out.println("   Ce client n'a aucun compte bancaire!!");
//			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}
	}
	
	public static void desactiverClient() {
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
	System.out.println("veuillez saisir l'Id client à désactiver");
	String idClt= in.nextLine();
		try { // Charger et configurer le driver de la base de donn�es.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);		
			// Connection en utilisant l'user directement dans la methode getConnection
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "sofianeT", "St241181");
			// Requette � executer
			String strQuery = "SELECT * FROM client where idClient='"+idClt+"'" ;
			
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);
			//System.out.println(listeServices.next());
			// Utilisation du ResultSet
			if (listeServices.next()) {
				
				strQuery = "UPDATE client SET actif=false where idClient='"+idClt+"'";
				stServices.executeUpdate(strQuery);
				System.out.println("Le client est désactivé avec succés");
			}else {
				System.out.println("client non trouvé!!!");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


		}
	}
	
	public static void supprimerCompte() {
		Scanner in = new Scanner (System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		System.out.println("veuillez saisir le numéro du compte à supprimer");
		int numCompte= in.nextInt();
		try { // Charger et configurer le driver de la base de donn�es.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);		
			// Connection en utilisant l'user directement dans la methode getConnection
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "sofianeT", "St241181");
			// Requette � executer
			String strQuery = "SELECT * FROM compte where numCompte='"+numCompte+"'" ;
			
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);
			//System.out.println(listeServices.next());
			// Utilisation du ResultSet
			if (listeServices.next()) {
				
				strQuery = "delete FROM compte where numCompte='"+numCompte+"'" ;
				stServices.executeUpdate(strQuery);
				System.out.println("Le compte est supprimé ");
			}else {
				System.out.println("compte non trouvé!!!");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
}
