package fr.afpa.control;

import java.util.regex.Pattern;
import java.time.LocalDate;
import java.util.Date;
import java.util.regex.Matcher;
import fr.afpa.beans.Agence;
import fr.afpa.beans.Client;
import fr.afpa.beans.Compte;
import fr.afpa.dao.Dao;
import fr.afpa.model.Model;
import fr.afpa.vue.Vues;
/**
 * classe control
 * @author sofiane T
 *
 */
public class Control {
	/**
	 * méthode control de création d'agence
	 *
	 */
public static boolean creerAgence() {

		 //récupération des données saisies par l'utilisateur		
		String tab[]=Vues.CreerAgence().split("//");
	if (Pattern.matches(" *",tab[0])) {
		return false;
	}else {
		Agence agence = new Agence(tab[0],tab[1]);
		Model.creerAgence(agence);
		return true;
	}
}

public static boolean creerClient() {
	String tab[]= Vues.CreerClient();
	for (int i = 0; i < tab.length; i++) {
		if(tab[i].isEmpty()) {
			i=tab.length;
			return false;
		}
	}
	if (!Pattern.matches("[A-Z]{2}[0-9]{6}",tab[0])) {
		return false;
	}
	if (!Pattern.matches("[a-z][a-zA-Z_0-9]*.[a-zA-Z_0-9]+@[a-z0-9]{2,7}.[a-z0-9]{2,5}",tab[5])) {
		return false;
	}
	if(!Pattern.matches("(19|20)[0-9]{2}-[0-9]{2}-[0-9]{2}",tab[3])){
		return false;
	}
	Client client = new Client(tab[0],tab[1],tab[2],LocalDate.parse(tab[3]),tab[4],tab[5]);
	Model.creerClient(client);
	return true;
}
public static boolean creerCompte() {
	String [] tab=Vues.creerCompte();
	Client clt= new Client(tab[0]);
	int cpt=Dao.verifCompteClient(clt);
	if(cpt==-1||cpt>=3) {
		return false;
	}else {
		Compte compte= new Compte(tab[0],Integer.parseInt(tab[1]),Float.parseFloat(tab[2]));
		Model.creerCompte(compte);
		return true;
	}

}
//public static boolean rechercherCompte() {
// int val= Vues.rechercherCompte();
//}	
}