package fr.afpa.beans;

public class Agence {
private int codeAgence;
private String nomAgence;
private String adressAgence;
public Agence(String nomAgence, String adressAgence) {
	super();
	//this.codeAgence = codeAgence;
	this.nomAgence = nomAgence;
	this.adressAgence = adressAgence;
}
public int getCodeAgence() {
	return codeAgence;
}
public void setCodeAgence(int codeAgence) {
	this.codeAgence = codeAgence;
}
public String getNomAgence() {
	return nomAgence;
}
public void setNomAgence(String nomAgence) {
	this.nomAgence = nomAgence;
}
public String getAdressAgence() {
	return adressAgence;
}
public void setAdressAgence(String adressAgence) {
	this.adressAgence = adressAgence;
}

}
