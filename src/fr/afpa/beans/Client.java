package fr.afpa.beans;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class Client {
private String idClient;
private String nom;
private String prenom;
private LocalDate dateNaissance;
private String email;
private String adresse;
private ArrayList<Compte> listCompte= new ArrayList<Compte>();

public Client(String idClient) {
	this.idClient = idClient;

}
public Client(String idClient, String nom, String prenom, LocalDate dateNaissance,String adresse, String email) {
	super();
	this.idClient = idClient;
	this.nom = nom;
	this.prenom = prenom;
	this.dateNaissance = dateNaissance;
	this.email = email;
	this.adresse=adresse;
}
public String getIdClient() {
	return idClient;
}
public void setIdClient(String idClient) {
	this.idClient = idClient;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public LocalDate getDateNaissance() {
	return dateNaissance;
}
public void setDateNaissance(LocalDate dateNaissance) {
	this.dateNaissance = dateNaissance;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public ArrayList<Compte> getListCompte() {
	return listCompte;
}
public void setListCompte(ArrayList<Compte> listCompte) {
	this.listCompte = listCompte;
}
public String getAdresse() {
	return adresse;
}
public void setAdresse(String adresse) {
	this.adresse = adresse;
}

}
