package fr.afpa.beans;

public class Compte {
private int numCompte;
private float solde;
private boolean dicouvert;
private String idClt;
private int idAgence;
public Compte( String idClt,int idAgence,float solde) {
	this.solde = solde;
	this.idClt=idClt;
	this.idAgence=idAgence;
}
public Compte(int numCompte, float solde, boolean dicouvert) {
	super();
	this.numCompte = numCompte;
	this.solde = solde;
	this.dicouvert = dicouvert;
}
public int getNumCompte() {
	return numCompte;
}
public void setNumCompte(int numCompte) {
	this.numCompte = numCompte;
}
public float getSolde() {
	return solde;
}
public void setSolde(float solde) {
	this.solde = solde;
}
public boolean isDicouvert() {
	return dicouvert;
}
public void setDicouvert(boolean dicouvert) {
	this.dicouvert = dicouvert;
}
public String getIdClt() {
	return idClt;
}
public void setIdClt(String idClt) {
	this.idClt = idClt;
}
public int getIdAgence() {
	return idAgence;
}
public void setIdAgence(int idAgence) {
	this.idAgence = idAgence;
}

}
