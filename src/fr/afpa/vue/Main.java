package fr.afpa.vue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

import fr.afpa.control.Control;
import fr.afpa.dao.Dao;
import fr.afpa.model.Model;


public class Main {

	public static void main(String[] args) {

		menu();
		
	}
	
	public static void menu() {
		Scanner in = new Scanner (System.in);
		String req;
		boolean fin =false;
		
		while (fin==false) {
			
			System.out.println("   +----------***--------------- MENU --------------***------------------+");
			System.out.println("   | 1- Créer une agence                                                 |");
			System.out.println("   | 2- Créer un client                                                  |");
			System.out.println("   | 3- Créer un compte bancaire                                         |");
			System.out.println("   | 4- Rechercher un compte (numéro de compte)                          |");
			System.out.println("   | 5- Rechercher un client(Nom, Numérode compte, identifiant de client)|");
			System.out.println("   | 6- Afficher la liste des comptes d'un client (identifiant client)   |");
			System.out.println("   | 7- Imprimer les infos client (identifiant client)                   |");
			System.out.println("   | 8- Désactiver un client                                             |");
			System.out.println("   | 9- Supprimer un compte                                              |");
			System.out.println("   | 10-Quitter le programme                                             |");
			System.out.println("   +---------------------------------------------------------------------+");
			System.out.println("   		***** Veuillez entrer votre choix *****");
			req = in.nextLine();
			switch (req){
			case "1": { 
						if(Control.creerAgence()) {
							System.out.println("agence créée avec succès");
						}else {
							System.out.println("erreur!!!");
						}		
							break;
						}
			case "2": { if(Control.creerClient()) {
				System.out.println("client créée avec succès");
			}else {
				System.out.println("erreur!!!");
			}		
				break;
			}
			case "3": {if(!Control.creerCompte()) {
				System.out.println("Impossible d'ajouter un compte pour ce client!!!");
			}else {
				System.out.println("Le compte est ajouté avec succès!!");
			}
				break;}
			case "4": { Model.rechercherCompte(); break;}
			case "5": { Model.rechercherClient(); break;}
			case "6": { Model.listCompteClient(); break;}
//			case "7": { Model.CreerCompte(); break;}
			case "8": { Model.desactiverClient(); break;}
			case "9": { Model.supprimerCompte(); break;}
			case "10": { fin=true; break;}
			default: {System.out.println("choix non disponible!!!"); break;}
			}
		}	
	}

}
