package fr.afpa.vue;

import java.util.Scanner;

/**
 * 
 * @author sofiane T
 * la classe Vues
 * 
 */
public class Vues {
	/**
	 * methode creerAgence
	 * @return une concatination des valeurs saisies
	 */
	public static String CreerAgence() {
		Scanner in = new Scanner (System.in);
		//ResultSet listeServices = null;
	System.out.println("veuillez saisir le nom de l'agence");
	String nom= in.nextLine();
	System.out.println("veuillez saisir l'adresse de l'agence");
	String adress= in.nextLine();
	if(nom.isEmpty()|| adress.isEmpty()) {	
		return " // ";
	}else {
		return nom+"//"+adress;
	}
		
	}
	/**
	 * methode creerClient
	 * @return
	 */
	public static String [] CreerClient() {
		Scanner in = new Scanner (System.in);
		System.out.println("veuillez saisir l'identifiant client ");
		String idClient= in.nextLine();
		System.out.println("veuillez saisir le nom client");
		String nom= in.nextLine();
		System.out.println("veuillez saisir le prenom client ");
		String prenom= in.nextLine();
		System.out.println("veuillez saisir la date de naissance ");
		String dateN= in.nextLine();
		System.out.println("veuillez saisir l'adresse client ");
		String adrClient= in.nextLine();
		System.out.println("veuillez saisir l'email client ");
		String email= in.nextLine();
		String tab[]= {idClient,nom,prenom,dateN,adrClient,email};
		return tab;
	}
	public static String [] creerCompte() {
		Scanner in = new Scanner (System.in);
		System.out.println("veuillez saisir l'id client ");
		String idClient= in.nextLine();
		System.out.println("veuillez saisir le num agence ");
		String numAgence= in.next();
		System.out.println("veuillez saisir le solde ");
		String solde= in.next();
		String tab[]= {idClient,numAgence, solde};
		return tab;
	}
//	public static int rechercherCompte() {
//		Scanner in= new Scanner(System.in);
//		System.out.println("veuillez saisir le numéro de compte");
//		int numCpte=in.nextInt();
//		return numCpte;		
//	}
}
