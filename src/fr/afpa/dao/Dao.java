package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import fr.afpa.beans.Agence;
import fr.afpa.beans.Client;
import fr.afpa.beans.Compte;
import fr.afpa.model.Model;

/**
 * Class Dao pour la connexion et CRUD dans bdd
 * @author Sofiane TAYEB 
 *
 */

public class Dao {
	
	/**
	 * method creerAgence 
	 *
	 */
	
	public static boolean creerAgence(Agence agence) {
	Connection conn = null;
	PreparedStatement ps = null;
	
	
	try { 
		
		 // Charger et configurer le driver de la base de données.
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);
		
		// Connection en utilisant l'user directement dans la methode getConnection
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "postgres", "cda-2020");
		// Requette à executer
		//String strQuery = "insert into agence values(nextval('seq_agence'),?,?)";
		ps =  conn.prepareStatement("insert into agence values(nextval('seq_agence'),?,?)");
		ps.setString(1,agence.getNomAgence());
		ps.setString(2,agence.getAdressAgence());
		ps.executeUpdate();
		return true;
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
		return false;
		
	} catch (SQLException e) {
		e.printStackTrace();
		return false;
	} finally {			
		// Fermeture du statement
		
		if (ps != null)
			try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture de la connection
		if (conn != null)
			try {

				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	}
	
	public static boolean creerClient(Client client) {

		Connection conn = null;
		PreparedStatement ps = null;
		//ResultSet listeServices = null;
		try { // Charger et configurer le driver de la base de donn�es.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
		
			// Connection en utilisant l'user directement dans la methode getConnection
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "cdaTest", "cda-2020");
			// Requette � executer
			ps = conn.prepareStatement("insert into client values(?,?,?,?,?,?)");
			ps.setString(1,client.getIdClient());
			ps.setString(2,client.getNom());
			ps.setString(3,client.getPrenom());
			ps.setDate(4,java.sql.Date.valueOf(client.getDateNaissance()));
			ps.setString(5,client.getAdresse());
			ps.setString(6,client.getEmail());


			ps.executeUpdate();
			return true;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {			
			// Fermeture du statement
			
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	public static int verifCompteClient(Client clt) {
		Connection conn = null;
		PreparedStatement ps = null;
		//Statement stClients = null;
		ResultSet listeServices = null;
		//ResultSet listeClients = null;
		
		//in.hasNextLine();
		//
		try { // Charger et configurer le driver de la base de données.
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			// Connection en utilisant l'user directement dans la methode getConnection
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "sofianeT", "St241181");
			// Requette à executer
			ps = conn.prepareStatement("SELECT * FROM Client where idClient=?");
			ps.setString(1,clt.getIdClient());
			listeServices = ps.executeQuery();
			if (listeServices.next()) {
				int cpt=0;
			ps = conn.prepareStatement("SELECT count(*) FROM compte where idClient=?");
			ps.setString(1,clt.getIdClient());
			listeServices = ps.executeQuery();
				if(listeServices.next()) {
			// Utilisation du ResultSet
				
					cpt= (listeServices.getInt(1));

			}
				System.out.println(cpt);
					return cpt;
			}else {
					return -1;
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return -1;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (ps != null)
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}
	}
	public static void creerCompte(Compte compte) {
	Connection conn = null;
	PreparedStatement ps = null;
	//ResultSet listeServices = null;
	try { // Charger et configurer le driver de la base de donn�es.
		String driverName = "org.postgresql.Driver";
		Class.forName(driverName);

		// Connection en utilisant l'user directement dans la methode getConnection
		conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA_BANK", "sofianeT", "St241181");
		// Requette � executer
		ps = conn.prepareStatement("insert into compte values(nextval('seq_compte'),'"+compte.getSolde()+"',true,'"+compte.getIdClt()+"','"+compte.getIdAgence()+"')");

		ps.executeUpdate();
		System.out.println("compte ajoutée avec succès");
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
		
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {			
		// Fermeture du statement
		
		if (stServices != null)
			try {
				stServices.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// Fermeture de la connection
		if (conn != null)
			try {

				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	}
}
